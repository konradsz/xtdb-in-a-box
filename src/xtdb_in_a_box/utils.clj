(ns xtdb-in-a-box.utils
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb :refer [q]]))

(defn easy-ingest
  "Uses XTDB put transaction to add a vector of documents to a specified
  node"
  [node docs]
  (xt/submit-tx node
                (vec (for [doc docs]
                       [::xt/put doc])))
  (xt/sync node))
