(ns xtdb-in-a-box.learn.space-adventure.match
  "Part 5 https://nextjournal.com/xtdb-tutorial/match"
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.utils :as u]
            [xtdb-in-a-box.xtdb :as xtdb :refer [q]]))

(def data
  [{:xt/id :gold-harmony
    :company-name "Gold Harmony"
    :seller? true
    :buyer? false
    :units/Au 10211
    :credits 51}

   {:xt/id :tombaugh-resources
    :company-name "Tombaugh Resources Ltd."
    :seller? true
    :buyer? false
    :units/Pu 50
    :units/N 3
    :units/CH4 92
    :credits 51}

   {:xt/id :encompass-trade
    :company-name "Encompass Trade"
    :seller? true
    :buyer? true
    :units/Au 10
    :units/Pu 5
    :units/CH4 211
    :credits 1002}

   {:xt/id :blue-energy
    :seller? false
    :buyer? true
    :company-name "Blue Energy"
    :credits 1000}])

(u/easy-ingest xtdb/xtdb-node data)

(defn stock-check
  [company-id item]
  {:result (q {:find '[name funds stock]
               :where ['[e :company-name name]
                       '[e :credits funds]
                       ['e item 'stock]]
               :in '[e]}
              company-id)
   :item item})

(defn format-stock-check
  [{:keys [result item] :as stock-check}]
  (for [[name funds commod] result]
    (str "Name: " name ", Funds: " funds ", " item " " commod)))

;;; ***** Valid match *****

(def expected-existing-blue-energy
  {:xt/id :blue-energy
   :seller? false
   :buyer? true
   :company-name "Blue Energy"
   :credits 1000})

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/match
    :blue-energy
    expected-existing-blue-energy]
   [::xt/put
    {:xt/id :blue-energy
     :seller? false
     :buyer? true
     :company-name "Blue Energy"
     :credits 900                 ;;; subtracted 100 from 1K
     :units/CH4 10}]              ;;; added 10 credits CH4

   [::xt/match
    :tombaugh-resources
    ;;; expected existing Tombaugh Resources
    {:xt/id :tombaugh-resources
     :company-name "Tombaugh Resources Ltd."
     :seller? true
     :buyer? false
     :units/Pu 50
     :units/N 3
     :units/CH4 92
     :credits 51}]
   [::xt/put
    {:xt/id :tombaugh-resources
     :company-name "Tombaugh Resources Ltd."
     :seller? true
     :buyer? false
     :units/Pu 50
     :units/N 3
     :units/CH4 82              ;;; Subtracted 10 CH4
     :credits 151}]])           ;;; Added 100 credits

(xt/sync xtdb/xtdb-node)

(format-stock-check (stock-check :tombaugh-resources :units/CH4))
;;;=> ["Name: Tombaugh Resources Ltd., Funds: 151, :units/CH4 82"]

(format-stock-check (stock-check :blue-energy :units/CH4))
;;;=> ["Name: Blue Energy, Funds: 900, :units/CH4 10"]

;;; The 100 credits move from Blue energy to Tombaugh Resources Ltd. and 10 units of Methane the other way.


;;; **** Invalid match *****

(format-stock-check (stock-check :gold-harmony :units/Au))
;;=> ("Name: Gold Harmony, Funds: 51, :units/Au 10211")

(format-stock-check (stock-check :encompass-trade :units/Au))
;;=> ("Name: Encompass Trade, Funds: 1002, :units/Au 10")

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/match
    :gold-harmony
    {:xt/id :gold-harmony
     :company-name "Gold Harmony"
     :seller? true
     :buyer? false
     :units/Au 10211
     :credits 51}]
   [::xt/put
    {:xt/id :gold-harmony
     :company-name "Gold Harmony"
     :seller? true
     :buyer? false
     :units/Au 211                                          ;;; subtracted 10k Au
     :credits 51}]

   [::xt/match
    :encompass-trade
    {:xt/id :encompass-trade
     :company-name "Encompass Trade"
     :seller? true
     :buyer? true
     :units/Au 10
     :units/Pu 5
     :units/CH4 211
     :credits
     100002                                                 ;;; invalid credits. Should be 1002
     ;1002                                                  ;;; valid amount of credits
     }]
   [::xt/put
    {:xt/id :encompass-trade
     :company-name "Encompass Trade"
     :seller? true
     :buyer? true
     :units/Au 10010                                        ;;; Added 10K Au
     :units/Pu 5
     :units/CH4 211
     :credits 1002}]])

(xt/sync xtdb/xtdb-node)

(format-stock-check (stock-check :gold-harmony :units/Au))

(format-stock-check (stock-check :encompass-trade :units/Au))