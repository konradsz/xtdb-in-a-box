(ns xtdb-in-a-box.learn.space-adventure.put
  "Part 2 https://nextjournal.com/xtdb-tutorial/put"
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb]))

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put
    {:xt/id :commodity/Pu
     :common-name "Plutonium"
     :type :element/metal
     :density 19.816
     :radioactive true}]

   [::xt/put
    {:xt/id :commodity/N
     :common-name "Nitrogen"
     :type :element/gas
     :density 1.2506
     :radioactive false}]

   [::xt/put
    {:xt/id :commodity/CH4
     :common-name "Methane"
     :type :molecule/gas
     :density 0.717
     :radioactive false}]])

(xt/sync xtdb/xtdb-node)

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put
    {:xt/id :stock/Pu
     :commod :commodity/Pu
     :weight-ton 21 }
    #inst "2115-02-13T18"]

   [::xt/put
    {:xt/id :stock/Pu
     :commod :commodity/Pu
     :weight-ton 23 }
    #inst "2115-02-14T18"]

   [::xt/put
    {:xt/id :stock/Pu
     :commod :commodity/Pu
     :weight-ton 22.2 }
    #inst "2115-02-15T18"]

   [::xt/put
    {:xt/id :stock/Pu
     :commod :commodity/Pu
     :weight-ton 24 }
    #inst "2115-02-18T18"]

   [::xt/put
    {:xt/id :stock/Pu
     :commod :commodity/Pu
     :weight-ton 24.9 }
    #inst "2115-02-19T18"]])

(xt/sync xtdb/xtdb-node)

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put
    {:xt/id :stock/N
     :commod :commodity/N
     :weight-ton 3 }
    #inst "2115-02-13T18"
    #inst "2115-02-19T18"]

   [::xt/put
    {:xt/id :stock/CH4
     :commod :commodity/CH4
     :weight-ton 92 }
    #inst "2115-02-15T18"
    #inst "2115-02-19T18"]])

(xt/sync xtdb/xtdb-node)

;;; {:commod :commodity/Pu, :weight-ton 21, :xt/id :stock/Pu}
(xt/entity (xt/db xtdb/xtdb-node #inst "2115-02-14") :stock/Pu)

;;; {:commod :commodity/Pu, :weight-ton 22.2, :xt/id :stock/Pu}
(xt/entity (xt/db xtdb/xtdb-node #inst "2115-02-18") :stock/Pu)
