(ns xtdb-in-a-box.learn.space-adventure.delete
  "Part 6 https://nextjournal.com/xtdb-tutorial/delete
  'Soft' Deletes a document at a given valid time. Historical version of the document will still be available."
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb]))

"The delete operation takes a valid eid with the option to include a start and end valid-time.

The document will be deleted as of the transaction time, or between the start and end valid-times if provided.

Historical versions of the document that fall outside of the valid-time window will be preserved."

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put {:xt/id :kaarlang/clients
              :clients [:encompass-trade]}
    #inst "2110-01-01T09"
    #inst "2111-01-01T09"]

   [::xt/put {:xt/id :kaarlang/clients
              :clients [:encompass-trade :blue-energy]}
    #inst "2111-01-01T09"
    #inst "2113-01-01T09"]

   [::xt/put {:xt/id :kaarlang/clients
              :clients [:blue-energy]}
    #inst "2113-01-01T09"
    #inst "2114-01-01T09"]

   [::xt/put {:xt/id :kaarlang/clients
              :clients [:blue-energy :gold-harmony :tombaugh-resources]}
    #inst "2114-01-01T09"
    #inst "2115-01-01T09"]])

(xt/sync xtdb/xtdb-node)

(defn show-history [date]
  (xt/entity-history
    (xt/db xtdb/xtdb-node date)
    :kaarlang/clients
    :desc
    {:with-docs? true}))

;;; Shows the latest record as first i.e. #inst "2115-01-01T09"

(show-history #inst "2116-01-01T09")

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/delete :kaarlang/clients #inst "2110-01-01" #inst "2116-01-01"]])

(xt/sync xtdb/xtdb-node)

;;; There are no :doc attached to transactions
(show-history #inst "2115-01-01T08")

;;; delete is 'soft' it's still possible to retrieve deleted documents
(xt/entity-history
  (xt/db xtdb/xtdb-node #inst "2115-01-01T08")
  :kaarlang/clients
  :desc
  {:with-docs? true
   :with-corrections? true})