(ns xtdb-in-a-box.learn.space-adventure.evict
  "Part 7 https://nextjournal.com/xtdb-tutorial/evict
  Permanently deletes a document at a given valid time."
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb]))

(def node (xt/start-node {}))

(xt/submit-tx
  node
  [[::xt/put
    {:xt/id :person/kaarlang
     :full-name "Kaarlang"
     :origin-planet "Mars"
     :identity-tag :KA01299242093
     :DOB #inst "2040-11-23"}]

   [::xt/put
    {:xt/id :person/ilex
     :full-name "Ilex Jefferson"
     :origin-planet "Venus"
     :identity-tag :IJ01222212454
     :DOB #inst "2061-02-17"}]

   [::xt/put
    {:xt/id :person/thadd
     :full-name "Thad Christover"
     :origin-moon "Titan"
     :identity-tag :IJ01222212454
     :DOB #inst "2101-01-01"}]

   [::xt/put
    {:xt/id :person/johanna
     :full-name "Johanna"
     :origin-planet "Earth"
     :identity-tag :JA012992129120
     :DOB #inst "2090-12-07"}]])

(xt/sync node)

(defn full-query
  [node]
  (xt/q
    (xt/db node)
    '{:find [(pull e [*])]
      :where [[e :xt/id id]]}))

(defn kerlang-history []
  (xt/entity-history (xt/db node)
                     :person/kaarlang
                     :desc
                     {:with-docs? true}))

(full-query node)

(kerlang-history)

(xt/submit-tx node [[::xt/evict :person/kaarlang]])

(xt/sync node)

(full-query node)

(kerlang-history)