(ns xtdb-in-a-box.learn.space-adventure.datalog
  "Part 3"
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.utils :as u]
            [xtdb-in-a-box.xtdb :as xtdb :refer [q]]))

(def data
  [{:xt/id :commodity/Pu
    :common-name "Plutonium"
    :type :element/metal
    :density 19.816
    :radioactive true}

   {:xt/id :commodity/N
    :common-name "Nitrogen"
    :type :element/gas
    :density 1.2506
    :radioactive false}

   {:xt/id :commodity/CH4
    :common-name "Methane"
    :type :molecule/gas
    :density 0.717
    :radioactive false}

   {:xt/id :commodity/Au
    :common-name "Gold"
    :type :element/metal
    :density 19.300
    :radioactive false}

   {:xt/id :commodity/C
    :common-name "Carbon"
    :type :element/non-metal
    :density 2.267
    :radioactive false}

   {:xt/id :commodity/borax
    :common-name "Borax"
    :IUPAC-name "Sodium tetraborate decahydrate"
    :other-names ["Borax decahydrate" "sodium borate" "sodium tetraborate" "disodium tetraborate"]
    :type :mineral/solid
    :appearance "white solid"
    :density 1.73
    :radioactive false}])

(u/easy-ingest xtdb/xtdb-node data)

(q '{:find [element]
     :where [[element :type :element/metal]]})

(=
  (xt/q (xt/db xtdb/xtdb-node)
        '{:find [element]
          :where [[element :type :element/metal]]})

  (xt/q (xt/db xtdb/xtdb-node)
        {:find '[element]
         :where '[[element :type :element/metal]]})

  (xt/q (xt/db xtdb/xtdb-node)
        (quote
          {:find [element]
           :where [[element :type :element/metal]]})))

;;; Return the name of metal elements.
(q '{:find [name]
     :where [[e :type :element/metal]
             [e :common-name name]]})

;;; Find density
(q '{:find [name rho]
     :where [[e :density rho]
             [e :common-name name]]})

;;; Pass arguments to query
(q '{:find [name]
     :where [[e :type type]
             [e :common-name name]]
     :in [type]}
   :element/metal)

(defn filter-type
  [type]
  (q '{:find [name]
       :where [[e :common-name name]
               [e :type type]]
       :in [type]}
     type))

(defn filter-appearance
  [description]
  (q '{:find [name IUPAC]
       :where [[e :common-name name]
               [e :IUPAC-name IUPAC]
               [e :appearance appearance]]
       :in [appearance]}
     description))

(filter-type :element/metal)

(filter-appearance "white solid")