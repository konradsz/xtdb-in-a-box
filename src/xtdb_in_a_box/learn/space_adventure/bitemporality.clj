(ns xtdb-in-a-box.learn.space-adventure.bitemporality
  "Part 4 https://nextjournal.com/xtdb-tutorial/bitemporality"
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb]))

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put
    {:xt/id :consumer/RJ29sUU
     :consumer-id :RJ29sUU
     :first-name "Jay"
     :last-name "Rose"
     :cover? true
     :cover-type :Full}
    #inst "2114-12-03"]])

(xt/sync xtdb/xtdb-node)

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put	;; (1)
    {:xt/id :consumer/RJ29sUU
     :consumer-id :RJ29sUU
     :first-name "Jay"
     :last-name "Rose"
     :cover? true
     :cover-type :Full}
    #inst "2113-12-03" ;; Valid time start
    #inst "2114-12-03"] ;; Valid time end

   [::xt/put  ;; (2)
    {:xt/id :consumer/RJ29sUU
     :consumer-id :RJ29sUU
     :first-name "Jay"
     :last-name "Rose"
     :cover? true
     :cover-type :Full}
    #inst "2112-12-03"
    #inst "2113-12-03"]

   [::xt/put	;; (3)
    {:xt/id :consumer/RJ29sUU
     :consumer-id :RJ29sUU
     :first-name "Jay"
     :last-name "Rose"
     :cover? false}
    #inst "2112-06-03"
    #inst "2112-12-02"]

   [::xt/put ;; (4)
    {:xt/id :consumer/RJ29sUU
     :consumer-id :RJ29sUU
     :first-name "Jay"
     :last-name "Rose"
     :cover? true
     :cover-type :Promotional}
    #inst "2111-06-03"
    #inst "2112-06-03"]])

(xt/sync xtdb/xtdb-node)

;; 1. This is the insurance that the customer had last year.
;;    Along with the start valid-time you use an end valid-time
;;    so as not to affect the most recent version of the document.

;; 2. This is the previous insurance plan. Again, you use a start and end valid-time.
;; 3. There was a period when the customer was not covered,
;; 4. and before that the customer was on a promotional plan.


(defn check-cover [time]
  (xt/q (xt/db xtdb/xtdb-node time)
        '{:find  [cover type]
          :where [[e :consumer-id :RJ29sUU]
                  [e :cover? cover]
                  [e :cover-type type]]}))
;;; Full cover
(check-cover #inst "2114-01-01")

;; Promotional cover
(check-cover #inst "2111-07-03")

;; no cover
(check-cover #inst "2112-07-03")