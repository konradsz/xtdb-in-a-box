(ns xtdb-in-a-box.learn.space-adventure.start
  "https://nextjournal.com/xtdb-tutorial"
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb]))

(def manifest
  {:xt/id :manifest
   :pilot-name "Johanna"
   :id/rocket "SB002-sol"
   :id/employee "22910x2"
   :badges "SETUP"
   :cargo ["stereo" "gold fish" "slippers" "secret note"]})

(xt/submit-tx xtdb/xtdb-node [[::xt/put manifest]])

(xt/sync xtdb/xtdb-node)

(xt/entity (xt/db xtdb/xtdb-node) :manifest)
