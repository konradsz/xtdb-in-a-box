(ns xtdb-in-a-box.learn.space-adventure.await
  "Part 8 https://nextjournal.com/xtdb-tutorial/await
  Permanently deletes a document at a given valid time."
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb]))

(def node (xt/start-node {}))

(def stats
  [{:body "Sun"
    :type "Star"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 109.3
    :volume 1305700
    :mass 33000
    :gravity 27.9
    :xt/id :Sun}
   {:body "Jupiter"
    :type "Gas Giant"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 10.97
    :volume 1321
    :mass 317.83
    :gravity 2.52
    :xt/id :Jupiter}
   {:body "Saturn"
    :type "Gas Giant"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius :volume
    :mass :gravity
    :xt/id :Saturn}
   {:body "Saturn"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 9.14
    :volume 764
    :mass 95.162
    :gravity 1.065
    :type "planet"
    :xt/id :Saturn}
   {:body "Uranus"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 3.981
    :volume 63.1
    :mass 14.536
    :gravity 0.886
    :type "planet"
    :xt/id :Uranus}
   {:body "Neptune"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 3.865
    :volume 57.7
    :mass 17.147
    :gravity 1.137
    :type "planet"
    :xt/id :Neptune}
   {:body "Earth"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 1
    :volume 1
    :mass 1
    :gravity 1
    :type "planet"
    :xt/id :Earth}
   {:body "Venus"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.9499
    :volume 0.857
    :mass 0.815
    :gravity 0.905
    :type "planet"
    :xt/id :Venus}
   {:body "Mars"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.532
    :volume 0.151
    :mass 0.107
    :gravity 0.379
    :type "planet"
    :xt/id :Mars}
   {:body "Ganymede"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.4135
    :volume 0.0704
    :mass 0.0248
    :gravity 0.146
    :type "moon"
    :xt/id :Ganymede}
   {:body "Titan"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.4037
    :volume 0.0658
    :mass 0.0225
    :gravity 0.138
    :type "moon"
    :xt/id :Titan}
   {:body "Mercury"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.3829
    :volume 0.0562
    :mass 0.0553
    :gravity 0.377
    :type "planet"
    :xt/id :Mercury}])

(xt/submit-tx node (mapv (fn [stat] [::xt/put stat]) stats))

(xt/sync node)

(xt/submit-tx
  node
  [[::xt/put
    {:body "Kepra-5"
     :units {:radius "Earth Radius"
             :volume "Earth Volume"
             :mass "Earth Mass"
             :gravity "Standard gravity (g)"}
     :radius 0.6729
     :volume 0.4562
     :mass 0.5653
     :gravity 1.4
     :type "planet"
     :xt/id :Kepra-5}]])

(xt/sync node)

(sort
  (xt/q
    (xt/db node)
    '{:find [g planet]
      :where [[planet :gravity g]]}))

(defn ingest-and-query
  [traveller-doc]
  (xt/submit-tx node [[::xt/put traveller-doc]])
  (xt/q
    (xt/db node)
    '{:find [n]
      :where [[id :passport-number n]]
      :in [id]}
    (:xt/id traveller-doc)))

;;; Submit operations in XTDB are asynchronous.
;;; Your query will not return the new data as it had not yet been indexed into XTDB.
(ingest-and-query
  {:xt/id :origin-planet/test-traveller
   :chosen-name "Test"
   :given-name "Test Traveller"
   :passport-number (java.util.UUID/randomUUID)
   :stamps []
   :penalties []})

;;; await-tx blocks until the node has indexed a transaction that is at or past the supplied tx.
(defn ingest-and-query-with-await
  "Ingests the given traveller's document into XTDB, returns the passport
  number once the transaction is complete."
  [traveller-doc]
  (xt/await-tx node
               (xt/submit-tx node [[::xt/put traveller-doc]]))
  (xt/q
    (xt/db node)
    '{:find [n]
      :where [[id :passport-number n]]
      :in [id]}
    (:xt/id traveller-doc)))

(ingest-and-query-with-await
  {:xt/id :origin-planet/new-test-traveller
   :chosen-name "Testy"
   :given-name "Test Traveller"
   :passport-number (java.util.UUID/randomUUID)
   :stamps []
   :penalties []})
