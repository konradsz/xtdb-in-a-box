(ns xtdb-in-a-box.learn.learn-datalog
  "https://nextjournal.com/try/learn-xtdb-datalog-today/learn-xtdb-datalog-today"
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb :refer [q]]))


(def my-docs
  [{:person/name "James Cameron",
    :person/born #inst "1954-08-16T00:00:00.000-00:00",
    :xt/id -100}
   {:person/name "Arnold Schwarzenegger",
    :person/born #inst "1947-07-30T00:00:00.000-00:00",
    :xt/id -101}
   {:person/name "Linda Hamilton",
    :person/born #inst "1956-09-26T00:00:00.000-00:00",
    :xt/id -102}
   {:person/name "Michael Biehn",
    :person/born #inst "1956-07-31T00:00:00.000-00:00",
    :xt/id -103}
   {:person/name "Ted Kotcheff",
    :person/born #inst "1931-04-07T00:00:00.000-00:00",
    :xt/id -104}
   {:person/name "Sylvester Stallone",
    :person/born #inst "1946-07-06T00:00:00.000-00:00",
    :xt/id -105}
   {:person/name "Richard Crenna",
    :person/born #inst "1926-11-30T00:00:00.000-00:00",
    :person/death #inst "2003-01-17T00:00:00.000-00:00",
    :xt/id -106}
   {:person/name "Brian Dennehy",
    :person/born #inst "1938-07-09T00:00:00.000-00:00",
    :xt/id -107}
   {:person/name "John McTiernan",
    :person/born #inst "1951-01-08T00:00:00.000-00:00",
    :xt/id -108}
   {:person/name "Elpidia Carrillo",
    :person/born #inst "1961-08-16T00:00:00.000-00:00",
    :xt/id -109}
   {:person/name "Carl Weathers",
    :person/born #inst "1948-01-14T00:00:00.000-00:00",
    :xt/id -110}
   {:person/name "Richard Donner",
    :person/born #inst "1930-04-24T00:00:00.000-00:00",
    :xt/id -111}
   {:person/name "Mel Gibson",
    :person/born #inst "1956-01-03T00:00:00.000-00:00",
    :xt/id -112}
   {:person/name "Danny Glover",
    :person/born #inst "1946-07-22T00:00:00.000-00:00",
    :xt/id -113}
   {:person/name "Gary Busey",
    :person/born #inst "1944-07-29T00:00:00.000-00:00",
    :xt/id -114}
   {:person/name "Paul Verhoeven",
    :person/born #inst "1938-07-18T00:00:00.000-00:00",
    :xt/id -115}
   {:person/name "Peter Weller",
    :person/born #inst "1947-06-24T00:00:00.000-00:00",
    :xt/id -116}
   {:person/name "Nancy Allen",
    :person/born #inst "1950-06-24T00:00:00.000-00:00",
    :xt/id -117}
   {:person/name "Ronny Cox",
    :person/born #inst "1938-07-23T00:00:00.000-00:00",
    :xt/id -118}
   {:person/name "Mark L. Lester",
    :person/born #inst "1946-11-26T00:00:00.000-00:00",
    :xt/id -119}
   {:person/name "Rae Dawn Chong",
    :person/born #inst "1961-02-28T00:00:00.000-00:00",
    :xt/id -120}
   {:person/name "Alyssa Milano",
    :person/born #inst "1972-12-19T00:00:00.000-00:00",
    :xt/id -121}
   {:person/name "Bruce Willis",
    :person/born #inst "1955-03-19T00:00:00.000-00:00",
    :xt/id -122}
   {:person/name "Alan Rickman",
    :person/born #inst "1946-02-21T00:00:00.000-00:00",
    :xt/id -123}
   {:person/name "Alexander Godunov",
    :person/born #inst "1949-11-28T00:00:00.000-00:00",
    :person/death #inst "1995-05-18T00:00:00.000-00:00",
    :xt/id -124}
   {:person/name "Robert Patrick",
    :person/born #inst "1958-11-05T00:00:00.000-00:00",
    :xt/id -125}
   {:person/name "Edward Furlong",
    :person/born #inst "1977-08-02T00:00:00.000-00:00",
    :xt/id -126}
   {:person/name "Jonathan Mostow",
    :person/born #inst "1961-11-28T00:00:00.000-00:00",
    :xt/id -127}
   {:person/name "Nick Stahl",
    :person/born #inst "1979-12-05T00:00:00.000-00:00",
    :xt/id -128}
   {:person/name "Claire Danes",
    :person/born #inst "1979-04-12T00:00:00.000-00:00",
    :xt/id -129}
   {:person/name "George P. Cosmatos",
    :person/born #inst "1941-01-04T00:00:00.000-00:00",
    :person/death #inst "2005-04-19T00:00:00.000-00:00",
    :xt/id -130}
   {:person/name "Charles Napier",
    :person/born #inst "1936-04-12T00:00:00.000-00:00",
    :person/death #inst "2011-10-05T00:00:00.000-00:00",
    :xt/id -131}
   {:person/name "Peter MacDonald", :xt/id -132}
   {:person/name "Marc de Jonge",
    :person/born #inst "1949-02-16T00:00:00.000-00:00",
    :person/death #inst "1996-06-06T00:00:00.000-00:00",
    :xt/id -133}
   {:person/name "Stephen Hopkins", :xt/id -134}
   {:person/name "Ruben Blades",
    :person/born #inst "1948-07-16T00:00:00.000-00:00",
    :xt/id -135}
   {:person/name "Joe Pesci",
    :person/born #inst "1943-02-09T00:00:00.000-00:00",
    :xt/id -136}
   {:person/name "Ridley Scott",
    :person/born #inst "1937-11-30T00:00:00.000-00:00",
    :xt/id -137}
   {:person/name "Tom Skerritt",
    :person/born #inst "1933-08-25T00:00:00.000-00:00",
    :xt/id -138}
   {:person/name "Sigourney Weaver",
    :person/born #inst "1949-10-08T00:00:00.000-00:00",
    :xt/id -139}
   {:person/name "Veronica Cartwright",
    :person/born #inst "1949-04-20T00:00:00.000-00:00",
    :xt/id -140}
   {:person/name "Carrie Henn", :xt/id -141}
   {:person/name "George Miller",
    :person/born #inst "1945-03-03T00:00:00.000-00:00",
    :xt/id -142}
   {:person/name "Steve Bisley",
    :person/born #inst "1951-12-26T00:00:00.000-00:00",
    :xt/id -143}
   {:person/name "Joanne Samuel", :xt/id -144}
   {:person/name "Michael Preston",
    :person/born #inst "1938-05-14T00:00:00.000-00:00",
    :xt/id -145}
   {:person/name "Bruce Spence",
    :person/born #inst "1945-09-17T00:00:00.000-00:00",
    :xt/id -146}
   {:person/name "George Ogilvie",
    :person/born #inst "1931-03-05T00:00:00.000-00:00",
    :xt/id -147}
   {:person/name "Tina Turner",
    :person/born #inst "1939-11-26T00:00:00.000-00:00",
    :xt/id -148}
   {:person/name "Sophie Marceau",
    :person/born #inst "1966-11-17T00:00:00.000-00:00",
    :xt/id -149}
   {:movie/title "The Terminator",
    :movie/year 1984,
    :movie/director -100,
    :movie/cast [-101 -102 -103],
    :movie/sequel -207,
    :xt/id -200}
   {:movie/title "First Blood",
    :movie/year 1982,
    :movie/director -104,
    :movie/cast [-105 -106 -107],
    :movie/sequel -209,
    :xt/id -201}
   {:movie/title "Predator",
    :movie/year 1987,
    :movie/director -108,
    :movie/cast [-101 -109 -110],
    :movie/sequel -211,
    :xt/id -202}
   {:movie/title "Lethal Weapon",
    :movie/year 1987,
    :movie/director -111,
    :movie/cast [-112 -113 -114],
    :movie/sequel -212,
    :xt/id -203}
   {:movie/title "RoboCop",
    :movie/year 1987,
    :movie/director -115,
    :movie/cast [-116 -117 -118],
    :xt/id -204}
   {:movie/title "Commando",
    :movie/year 1985,
    :movie/director -119,
    :movie/cast [-101 -120 -121],
    :trivia
    "In 1986, a sequel was written with an eye to having\n  John McTiernan direct. Schwarzenegger wasn't interested in reprising\n  the role. The script was then reworked with a new central character,\n  eventually played by Bruce Willis, and became Die Hard",
    :xt/id -205}
   {:movie/title "Die Hard",
    :movie/year 1988,
    :movie/director -108,
    :movie/cast [-122 -123 -124],
    :xt/id -206}
   {:movie/title "Terminator 2: Judgment Day",
    :movie/year 1991,
    :movie/director -100,
    :movie/cast [-101 -102 -125 -126],
    :movie/sequel -208,
    :xt/id -207}
   {:movie/title "Terminator 3: Rise of the Machines",
    :movie/year 2003,
    :movie/director -127,
    :movie/cast [-101 -128 -129],
    :xt/id -208}
   {:movie/title "Rambo: First Blood Part II",
    :movie/year 1985,
    :movie/director -130,
    :movie/cast [-105 -106 -131],
    :movie/sequel -210,
    :xt/id -209}
   {:movie/title "Rambo III",
    :movie/year 1988,
    :movie/director -132,
    :movie/cast [-105 -106 -133],
    :xt/id -210}
   {:movie/title "Predator 2",
    :movie/year 1990,
    :movie/director -134,
    :movie/cast [-113 -114 -135],
    :xt/id -211}
   {:movie/title "Lethal Weapon 2",
    :movie/year 1989,
    :movie/director -111,
    :movie/cast [-112 -113 -136],
    :movie/sequel -213,
    :xt/id -212}
   {:movie/title "Lethal Weapon 3",
    :movie/year 1992,
    :movie/director -111,
    :movie/cast [-112 -113 -136],
    :xt/id -213}
   {:movie/title "Alien",
    :movie/year 1979,
    :movie/director -137,
    :movie/cast [-138 -139 -140],
    :movie/sequel -215,
    :xt/id -214}
   {:movie/title "Aliens",
    :movie/year 1986,
    :movie/director -100,
    :movie/cast [-139 -141 -103],
    :xt/id -215}
   {:movie/title "Mad Max",
    :movie/year 1979,
    :movie/director -142,
    :movie/cast [-112 -143 -144],
    :movie/sequel -217,
    :xt/id -216}
   {:movie/title "Mad Max 2",
    :movie/year 1981,
    :movie/director -142,
    :movie/cast [-112 -145 -146],
    :movie/sequel -218,
    :xt/id -217}
   {:movie/title "Mad Max Beyond Thunderdome",
    :movie/year 1985,
    :movie/director [-142 -147],
    :movie/cast [-112 -148],
    :xt/id -218}
   {:movie/title "Braveheart",
    :movie/year 1995,
    :movie/director [-112],
    :movie/cast [-112 -149],
    :xt/id -219}])

(xt/submit-tx xtdb/xtdb-node (for [doc my-docs]
                               [::xt/put doc]))

(xt/sync xtdb/xtdb-node)

(xt/q (xt/db xtdb/xtdb-node)
      '{:find [title]
        :where [[_ :movie/title title]]})

;;; *** Simple queries ***

(q '{:find [title]
     :where [[_ :movie/title title]]})

(q '{:find [e]
     :where [[e :person/name _]]})

;;; Find the entity ids of movies made in 1987
(q '{:find [e]
     :where [[e :movie/year 1987]]})

;;; Find the entity-id and titles of movies in the database
(q '{:find [e title]
     :where [[e :movie/title title]]})

;;;  Find the name of all people in the database
(q '{:find [name]
     :where [[_ :person/name name]]})

;;; Find movie titles made in 1985
(q '{:find [title]
     :where [[e :movie/title title]
             [e :movie/year 1985]]})

;;; What year was "Alien" released?
(q '{:find [year]
     :where [[e :movie/year year]
             [e :movie/title "Alien"]]})

;;; Who directed RoboCop?
(q '{:find [name]
     :where [[m :movie/title "RoboCop"]
             [m :movie/director d]
             [d :person/name name]]})

;;; Find directors who have directed Arnold Schwarzenegger in a movie.
(q '{:find [name]
     :where [[m :movie/director d]
             [m :movie/cast p]
             [d :person/name name]
             [p :person/name "Arnold Schwarzenegger"]]})

;;; **** Parameterised queries ***

;;; scalar input
(q '{:find [title]
     :in [name]
     :where [[p :person/name name]
             [m :movie/cast p]
             [m :movie/title title]]}
   "Sylvester Stallone")

;;; Tuple input
(q '{:find [title]
     :in [[director actor]]
     :where [[d :person/name director]
             [a :person/name actor]
             [m :movie/director d]
             [m :movie/cast a]
             [m :movie/title title]]}
   ["James Cameron" "Arnold Schwarzenegger"])

;;; collection input
;;; You can use collection destructuring to implement a kind of *logical OR* in your query.

;;; Say you want to find all movies directed by either James Cameron **or** Ridley Scott
(q '{:find [title]
     :in [[director ...]]
     :where [[p :person/name director]
             [m :movie/director p]
             [m :movie/title title]]}
   ["James Cameron" "Ridley Scott"])

;;; relation input
;;; Relations - a set of tuples - are the most interesting and powerful of input types,
;;; since you can join external relations with the triples in your database.
(q '{:find [title box-office]
     :in [director [[title box-office]]]
     :where [[p :person/name director]
             [m :movie/director p]
             [m :movie/title title]]}
   "Ridley Scott"
   [["Die Hard" 140700000]
    ["Alien" 104931801]
    ["Lethal Weapon" 120207127]
    ["Commando" 57491000]])

;;; Find movie title by year
(q '{:find [title]
     :in [year]
     :where [[m :movie/year year]
             [m :movie/title title]]}
   1988)

;;; Given a list of movie titles, find the title and the year that movie was released.
(q '{:find [title year]
     :in [[title ...]]
     :where [[m :movie/title title]
             [m :movie/year year]]}
   ["Lethal Weapon" "Lethal Weapon 2" "Lethal Weapon 3"]
   )

;;; Find all movie `title`s where the `actor` and the `director` has worked together
(q '{:find [title]
     :in [actor director]
     :where [[m :movie/title title]
             [m :movie/director d]
             [d :person/name director]
             [m :movie/cast p]
             [p :person/name actor]]}
   "Michael Biehn"
   "James Cameron")

;;; Write a query that, given an actor name and a relation with movie-title/rating,
;;; finds the movie titles and corresponding rating for which that actor was a cast member.
(q '{:find  [title rating]
     :in    [actor [[title rating]]]
     :where [[m :movie/title title]
             [m :movie/cast p]
             [p :person/name actor]]}
   "Mel Gibson"
   [["Die Hard" 8.3]
    ["Alien" 8.5]
    ["Lethal Weapon" 7.6]
    ["Commando" 6.5]
    ["Mad Max Beyond Thunderdome" 6.1]
    ["Mad Max 2" 7.6]
    ["Rambo: First Blood Part II" 6.2]
    ["Braveheart" 8.4]
    ["Terminator 2: Judgment Day" 8.6]
    ["Predator 2" 6.1]
    ["First Blood" 7.6]
    ["Aliens" 8.5]
    ["Terminator 3: Rise of the Machines" 6.4]
    ["Rambo III" 5.4]
    ["Mad Max" 7.0]
    ["The Terminator" 8.1]
    ["Lethal Weapon 2" 7.1]
    ["Predator" 7.8]
    ["Lethal Weapon 3" 6.6]
    ["RoboCop" 7.5]])


;;; **** Predicates ***

;;; Find all movies released before 1984
(q '{:find [title]
     :where [[m :movie/title title]
             [m :movie/year year]
             [(< year 1984)]]})

(q '{:find [name]
     :where [[p :person/name name]
             [(clojure.string/starts-with? name "M")]]})

;;; Find movies older than a certain year (inclusive)
(q '{:find [title]
     :in [year]
     :where [[m :movie/title title]
             [m :movie/year movie-year]
             [(<= movie-year year)]]}
   1979)

;;; Find actors older than Danny Glover
(q '{:find [actor]
     :where [[p :person/born person-born]
             [p :person/name actor]
             [d :person/name "Danny Glover"]
             [d :person/born danny-born]
             [_ :movie/cast p]
             [(< person-born danny-born )]]})

;;; Find movies newer than `year` (inclusive) and has a `rating` higher than the one supplied
(q '{:find [title]
     :in [year rating [[title r]]]
     :where [[m :movie/title title]
             [m :movie/year y]
             ]}
   1990
   8.0
   [["Die Hard" 8.3]
    ["Alien" 8.5]
    ["Lethal Weapon" 7.6]
    ["Commando" 6.5]
    ["Mad Max Beyond Thunderdome" 6.1]
    ["Mad Max 2" 7.6]
    ["Rambo: First Blood Part II" 6.2]
    ["Braveheart" 8.4]
    ["Terminator 2: Judgment Day" 8.6]
    ["Predator 2" 6.1]
    ["First Blood" 7.6]
    ["Aliens" 8.5]
    ["Terminator 3: Rise of the Machines" 6.4]
    ["Rambo III" 5.4]
    ["Mad Max" 7.0]
    ["The Terminator" 8.1]
    ["Lethal Weapon 2" 7.1]
    ["Predator" 7.8]
    ["Lethal Weapon 3" 6.6]
    ["RoboCop" 7.5]]
   )

;;; **** Transformation functions ***

; Transformation functions** are pure (side-effect free) functions which can be used in queries
; as "function expression" predicates to transform values and bind their results to new logic variables.

; Say, for example, there exists an attribute `:person/born` with type `:db.type/instant`.
; Given the birthday, it's easy to calculate the (very approximate) age of a person
(defn age [^java.util.Date birthday ^java.util.Date today]
  (quot (- (.getTime today)
           (.getTime birthday))
        (* 1000 60 60 24 365)))

(q '{:find [age]
     :in [name today]
     :where [[p :person/name name]
             [p :person/born born]
             [(xtdb-in-a-box.learn-datalog.learn-datalog/age born today) age]]}
   "Tina Turner"
   (java.util.Date.))


; One thing to be aware of is that transformation functions can't be nested.
; Instead, you must bind intermediate results in temporary logic variables.

(q '{:find [name age]
     :in [age today]
     :where [[p :person/name name]
             [p :person/born born]
             [(xtdb-in-a-box.learn-datalog.learn-datalog/age born today) age]]}
   63
   #inst "2013-08-02T00:00:00.000-00:00")

; Find the names of people younger than Bruce Willis and their corresponding age.
(q '{:find  [name age]
     :in    [today]
     :where [[p :person/name name]
             [p :person/born born]
             [b :person/name "Bruce Willis"]
             [b :person/born bruce-born]
             [(xtdb-in-a-box.learn-datalog.learn-datalog/age born today) age]
             [(< bruce-born born)]]}
   #inst "2013-08-02T00:00:00.000-00:00")

;;; *** Aggregates ***

; `count` the number of movies in the database
(q '{:find [(count title)]
     :where [[m :movie/title title]]})

; Find the birth date of the oldest person in the database.
(q '{:find [(min born)]
     :where [[p :person/born born]]})

; Given a collection of actors and (the now familiar) ratings data. Find the average rating for each actor.
; The query should return the actor name and the `avg` rating.
(q '{:find  [name (avg rating)]
     :in    [[name ...] [[title rating]]]
     :where [[p :person/name name]
             [m :movie/cast p]
             [m :movie/title title]]}
   ["Sylvester Stallone" "Arnold Schwarzenegger" "Mel Gibson"]
   [["Die Hard" 8.3]
    ["Alien" 8.5]
    ["Lethal Weapon" 7.6]
    ["Commando" 6.5]
    ["Mad Max Beyond Thunderdome" 6.1]
    ["Mad Max 2" 7.6]
    ["Rambo: First Blood Part II" 6.2]
    ["Braveheart" 8.4]
    ["Terminator 2: Judgment Day" 8.6]
    ["Predator 2" 6.1]
    ["First Blood" 7.6]
    ["Aliens" 8.5]
    ["Terminator 3: Rise of the Machines" 6.4]
    ["Rambo III" 5.4]
    ["Mad Max" 7.0]
    ["The Terminator" 8.1]
    ["Lethal Weapon 2" 7.1]
    ["Predator" 7.8]
    ["Lethal Weapon 3" 6.6]
    ["RoboCop" 7.5]])

;;; *** Rules ***

; Rules are the means of abstraction in Datalog. You can abstract away reusable parts of your queries into rules,
; give them meaningful names and forget about the implementation details

; The first vector is called the *head* of the rule where the first symbol is the name of the rule.
;     (actor-movie name title)
; The rest of the rule is called the *body*.
;    [p :person/name name]
;    [m :movie/cast p]
;    [m :movie/title title]

(q '{:find [name]
     :where [(actor-movie name "The Terminator")]
     :rules [[(actor-movie name title)
              [p :person/name name]
              [m :movie/cast p]
              [m :movie/title title]]]})

;You can think of a rule as a kind of function, but remember that this is logic programming, so we can use the same rule to:

;* find movie titles given an actor name, and
;* find actor names given a movie title.

;Put another way, we can use both `name` and `title` in `(actor-movie name title)` for input as well as for output.
; If we provide values for neither, we'll get all the possible combinations in the database.
; If we provide values for one or both, it will constrain the result returned by the query as you'd expect.

; You can write any number of rules, collect them in a vector, and pass them to the query engine using the `:rules` key.

 ;Rules can also be used as another tool to write *logical OR* queries, as the same rule name can be used several times:
(comment
  [[(associated-with person movie)
    [movie :movie/cast person]]
   [(associated-with person movie)
    [movie :movie/director person]]])

; Subsequent rule definitions will only be used if the ones preceding it aren't satisfied.

;Using this rule, we can find both directors and cast members very easily:

(q '{:find [name]
     :where [[m :movie/title "Predator"]
             (associated-with p m)
             [p :person/name name]]
     :rules [[(associated-with person movie)
              [movie :movie/cast person]]
             [(associated-with person movie)
              [movie :movie/director person]]]})

; Write a rule `(movie-year title year)` where `title` is the title of some movie and `year` is that movie's release year.
(q '{:find [title]
     :where [(movie-year title 1991)]
     :rules [[(movie-year title year)
              [m :movie/title title]
              [m :movie/year year]]]})

; Two people are friends if they have worked together in a movie.
;  Write a rule `(friends p1 p2)` where `p1` and `p2` are person entities.
(q '{:find [friend]
     :in [name]
     :where [[p1 :person/name name]
             (friends p1 p2)
             [p2 :person/name friend]]
     :rules [[(friends p1 p2)
              [m :movie/cast p1]
              [m :movie/cast p2]
              [(not= p1 p2)]]
             [(friends p1 p2)
              [m :movie/cast p1]
              [m :movie/director p2]
              [(not= p1 p2)]]
             [(friends p1 p2)
              [m :movie/director p1]
              [m :movie/cast p2]
              [(not= p1 p2)]]]
     }
   "Sigourney Weaver")