(ns xtdb-in-a-box.learn.language-reference.datalog-queries
  "https://docs.xtdb.com/language-reference/datalog-queries"
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb :refer [q]]))

(def products
  [{:product/name "Black Jacket"
    :product/net-price 12
    :product/type :jacket
    :product/tax-rate 15
    :product/stock-available 4
    :product/color :black
    :xt/id :jacket-black}
   {:product/name "Red Jacket"
    :product/net-price 10
    :product/type :jacket
    :product/related #{"Black Jacket"}
    :product/tax-rate 13
    :product/stock-available 0
    :product/color :red
    :xt/id :jacket-red}
   {:product/name "Green Jacket"
    :product/net-price 9
    :product/type :jacket
    :product/related #{"Black Jacket"}
    :product/tax-rate 15
    :product/stock-available 3
    :product/color :green
    :xt/id :jacket-green}
   {:product/name "Red Trousers"
    :product/net-price 18
    :product/type :trousers
    :product/tax-rate 20
    :product/related #{"Blue Trousers" "Black Trousers"}
    :product/stock-available 3
    :product/color :red
    :xt/id :trousers-red}
   {:product/name "Blue Trousers"
    :product/net-price 50
    :product/type :trousers
    :product/tax-rate 30
    :product/stock-available 8
    :product/color :blue
    :xt/id :trousers-blue}
   {:product/name "Black Trousers"
    :product/type :trousers
    :product/net-price 18
    :product/stock-available 1
    :product/color :black
    :xt/id :trousers-black}
   {:color/color "Black"
    :xt/id :black}
   {:color/color "Blue"
    :xt/id :blue}
   {:color/color "Red"
    :xt/id :red}
   {:color/color "Green"
    :xt/id :green}
   {:xt/id :foo
    :bar {:baz {:qux {:qaz 123}}}}])

(xt/submit-tx xtdb/xtdb-node (mapv (fn [product] [::xt/put product]) products))

(q '{:find [product-name
           (* net-price (+ 1.0 tax-rate))
           (if (> stock 0)
             "in stock"
             "out of stock")]
    :where [[p :product/name product-name]
            [p :product/net-price net-price]
            [p :product/tax-rate tax-rate]
            [p :product/stock-available stock]]})

(q '{:find [(sum ?heads)
            (min ?heads)
            (max ?heads)
            (count ?heads)
            (count-distinct ?heads)]
     :in [[[?monster ?heads]]]}
   [["Cerberus" 3]
    ["Medusa" 1]
    ["Cyclops" 1]
    ["Chimera" 1]])

;;; TODO custom aggregates

;;;; Using Pull API

;; with just 'query':
(q '{:find [?uid ?product-name ?net-price ?product-color]
     :where [[?product :xt/id ?uid]
             [?product :product/name ?product-name]
             [?product :product/color ?product-color]
             [?product :product/net-price ?net-price]]})

;; using `pull`:
(q '{:find [(pull ?product [:product/name :product/net-price :product/color])]
     :where [[?product :xt/id ?uid]]})

(q '{:find [(pull ?product [*])]
     :where [[?product :xt/id :trousers]]})

(xt/pull
  (xt/db xtdb/xtdb-node)
  [:product/name :product/stock-available]
  :jacket-black)

(xt/pull-many
  (xt/db xtdb/xtdb-node)
  [:product/name :product/net-price]
  [:jacket-black :jacket-red])

;;; Joins

;; Using a query
(q '{:find [?product-name ?color]
     :where [[?product :product/name ?product-name]
             [?product :product/color ?product-color]
             [?product-color :color/color ?color]]})

;; Using pull
(q '{:find [(pull ?product [:product/name {:product/color [:color/color]}])]
     :where [[?product :xt/id ?uid]]})

;;; We can also navigate in the reverse direction, looking for entities that refer to this one, by prepending _ to the attribute name:
(q '{:find [(pull ?color [:color/color {:product/_color [:product/name :product/net-price]}])]
     :where [[?color :color/color]]})

(q '{:find [(pull ?color [:color/color {(:product/_color {:as :products}) [:product/name :product/net-price]}])]
     :where [[?color :color/color]]})

(q '{:find [(pull ?color [:color/color {(:product/_color {:as :products :limit 1}) [:product/name :product/net-price]}])]
     :where [[?color :color/color]]})

(q '{:find [(pull ?color [:color/color {(:product/_color {:as :products :into []}) [:product/name :product/net-price]}])]
     :where [[?color :color/color]]})

;;; TODO :default not working?
(q '{:find [(pull ?color [:color/color {(:product/_color {:as :products :default "Brak"}) [:product/name :product/tax-rate]}])]
     :where [[?color :color/color]]})

;;; Returning maps not tuples (vectors)
(q '{:find [?color-name ?product-name]
     :keys [color name]
     :where [[?product :product/name ?product-name]
             [?product :product/color ?color]
             [?color :color/color ?color-name]]})

(q '{:find [(pull ?product [:product/name {:product/color [:color/color]}])]
     :keys [name]
     :where [[?product :xt/id ?uid]]})

;;; ******* WHERE ********

(q '{:find [?name ?color]
     :where [[?p :product/name ?name]
             [?p :product/color ?c]
             [?c :color/color ?color]]})

;;; The NOT clause rejects a graph if all the clauses within it are true.
;; It's an AND
(q '{:find  [?name]
     :where [[?p :product/name ?name]
             [?p :product/type :jacket]
             (not [?p :product/color :red]
                  [?p :product/stock-available 0])]})

(q '{:find [?name]
     :where [[?p :product/name ?name]
             (not-join [?p]
                       [?p :product/type :jacket]
                       [?p :product/color :red])]})

(q '{:find [?name ?av]
     :where [[?p :product/name ?name]
             [?p :product/stock-available ?av]
             (or [?p :product/color :red]
                 [?p :product/type :jacket])]})

(q '{:find  [?name ?av]
     :where [[?p :product/name ?name]
             [?p :product/stock-available ?av]
             (or [?p :product/color :red]
                 (and [?p :product/type :jacket]
                      ;;; For predicates see or-join
                      [?p :product/stock-available 4]))]})

(q '{:find [?name ?av]
     :where [[?p :product/name ?name]
             [?p :product/stock-available ?av]
             (or-join [?p]
                      (and [?p :product/type :jacket]
                           [?p :product/stock-available ?av]
                           [(> ?av 3)])
                      [?p :product/color :red])]})

;;; ***** Ordering and pagination *****

(q '{:find [?name ?av]
     :where [[?p :product/name ?name]
             [?p :product/stock-available ?av]]
     :order-by [[?av :desc] [?name :asc]]})

;;; Use of :order-by will require that results are fully-realised by the query engine,
;;; however this happens transparently, and it will automatically spill to disk when sorting large numbers of results.

(q '{:find [?name ?av]
     :where [[?p :product/name ?name]
             [?p :product/stock-available ?av]]
     :order-by [[?av :desc]]
     :limit 3})

(q '{:find [?name ?av]
     :where [[?p :product/name ?name]
             [?p :product/stock-available ?av]]
     :order-by [[?av :desc]]
     :limit 3
     :offset 2})

;;; Ordered results are returned as bags, not sets, so you may want to deduplicate
;;; consecutive identical result tuples (e.g. using clojure.core/dedupe or similar).

(dedupe (q '{:find [?name ?av]
             :where [[?p :product/name ?name]
                     [?p :product/stock-available ?av]]
             :order-by [[?av :desc] [?name :asc]]}))

;;; order by with an aggregate
(q '{:find [(sum ?av)
            ?type]
     :where [[?p :product/stock-available ?av]
             [?p :product/type ?type]]
     :order-by [[(sum ?av) :desc]]})

(q '{:find  [?type ?name]
     :where [[?p :product/name ?name]
             [?p :product/type ?type]
             (jacket? ?p)]
     :rules [[(jacket? ?p)
              [?p :product/type :jacket]]]})

(q '{:find  [?type ?name ?color]
     :where [[?p :product/name ?name]
             [?p :product/type ?type]
             [?p :product/color ?color]
             (jacket-or-red? ?p)]
     :rules [[(jacket-or-red? ?p)
              [?p :product/type :jacket]]
             [(jacket-or-red? ?p)
              [?p :product/color :red]]]
     :order-by [[?type :asc]]})

;;; timeout in milliseconds
(q '{:find [(sum ?av)
            ?type]
     :where [[?p :product/stock-available ?av]
             [?p :product/type ?type]]
     :timeout 1})

;;; ***** Time travel *****

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put
    {:xt/id :malcolm :name "Malcolm" :last-name "Sparks"}
    #inst "1986-10-22"]])

(xt/submit-tx
  xtdb/xtdb-node
  [[::xt/put
    {:xt/id :malcolm :name "Malcolma" :last-name "Sparks"}
    #inst "1986-10-24"]])

(def malcolm-q
  '{:find [e]
    :where [[e :name "Malcolma"]
            [e :last-name "Sparks"]]})

(xt/q (xt/db xtdb/xtdb-node #inst "1986-10-23") malcolm-q)

(xt/q (xt/db xtdb/xtdb-node) malcolm-q)

;;; ***** joins *****

;;; Query: "Join across entities on a single attribute"
(q '{:find [?p1 ?p2]
     :where [[?p1 :product/type ?type]
             [?p2 :product/type ?type]]})

;;; Query: "Join with two attributes, including a multivalued attribute"
(q '{:find [?name2]
     :where [[?p :xt/id :jacket-black]
             [?p2 :product/related ?name]
             [?p2 :product/name ?name2]
             [?p :product/name ?name]]})

(q '{:find [?name2]
     :where [[?p :xt/id :trousers-blue]
             [?p :product/name ?name]
             [?p2 :product/related ?name]
             [?p2 :product/name ?name2]]})

;;; ***** History API *****

(xt/entity-history (xt/db xtdb/xtdb-node) :malcolm :desc)

(xt/entity-history (xt/db xtdb/xtdb-node) :malcolm :desc {:with-docs? true})

(xt/entity-history (xt/db xtdb/xtdb-node) :malcolm :desc {:start-valid-time #inst "1986-10-23"
                                                          :with-docs? true})

(xt/entity-history (xt/db xtdb/xtdb-node) :malcolm :desc {:end-valid-time #inst "1986-10-23"
                                                          :with-docs? true})

(xt/entity-history (xt/db xtdb/xtdb-node) :malcolm :desc {:start-tx #inst "2022-10-31"
                                                          :with-docs? true})

(xt/entity-history (xt/db xtdb/xtdb-node) :malcolm :desc {:end-tx #inst "2222-12-31"
                                                          :with-docs? true})

(q '{:find [?color]
     :where [[?p :product/name "Red Jacket"]
             [(get-attr ?p :product/color) ?color]]})

(q '{:find  [?color]
     :where [[?p :product/name "Red Jacket"]
             [(get-attr ?p :product/color) [?color ...]]]})

(q '{:find [?color]
     :where [[?p :product/name "Red Jacket"]
             [?p :product/color ?color]]})

(q '{:find [?related]
     :where [[?p :product/name "Red Trousers"]
             [?p :product/related ?related]]})

;;; find entity that has missing? attribute e.g. :product/related

(q '{:find [?name]
     :where [[?p :product/name ?name]
             (not-join [?p]
                       [?p :product/related])]})

(q '{:find [?v]
     :where [[:foo :bar bar-val]
             [(get-in bar-val [:baz :qux :qaz]) ?v]]})

(q '{:find [v]
     :in [k1 k2]
     :where [[(vector k1 k2 :qaz) ks]
             [:foo :bar bar-val]
             [(get-in bar-val ks) v]]}
   :baz :qux)