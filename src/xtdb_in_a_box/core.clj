(ns xtdb-in-a-box.core
  (:require [xtdb.api :as xt]
            [xtdb-in-a-box.xtdb :as xtdb]))

(comment

  (xt/submit-tx xtdb/xtdb-node [[::xt/put
                                 {:xt/id "hi2u"
                                  :user/name "zig"}]])

  (xt/q (xt/db xtdb/xtdb-node) '{:find  [e]
                                 :where [[e :user/name "zig"]]})

  )