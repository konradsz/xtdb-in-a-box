(defproject xtdb-in-a-box "0.1.0-SNAPSHOT"
  :description "Learning about xtdb"
  :url "https://xtdb.com/"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [com.xtdb/xtdb-core "1.22.0"]
                 [com.xtdb/xtdb-rocksdb "1.22.0"]]
  :repl-options {:init-ns xtdb-in-a-box.core})
